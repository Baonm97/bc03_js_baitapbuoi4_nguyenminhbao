var checkTriangle = function () {
  var a = document.getElementById("canh-a").value * 1;
  var b = document.getElementById("canh-b").value * 1;
  var c = document.getElementById("canh-c").value * 1;
  var t = null;
  if (a == b && a == c && b == c) {
    t = "Tam giac deu";
  } else if (a == b || a == c || b == c) {
    t = "Tam giac can";
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    t = "Tam giac vuong";
  } else {
    t = "Tam giac thuong";
  }
  document.getElementById("ketQua").innerHTML = `${t}`;
};
