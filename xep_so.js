var sapXep = function () {
  var soThuNhat = document.getElementById("so-thu-nhat").value * 1;
  var soThuHai = document.getElementById("so-thu-hai").value * 1;
  var soThuBa = document.getElementById("so-thu-ba").value * 1;
  var sapXep = null;
  if (soThuNhat > soThuHai && soThuNhat > soThuBa && soThuHai > soThuBa) {
    sapXep = `${soThuBa}, ${soThuHai}, ${soThuNhat} `;
  } else if (
    soThuNhat > soThuHai &&
    soThuNhat > soThuBa &&
    soThuHai < soThuBa
  ) {
    sapXep = `${soThuHai}, ${soThuBa}, ${soThuNhat}`;
  } else if (
    soThuNhat < soThuHai &&
    soThuNhat > soThuBa &&
    soThuHai > soThuBa
  ) {
    sapXep = `${soThuBa}, ${soThuNhat}, ${soThuHai}`;
  } else if (
    soThuNhat < soThuHai &&
    soThuNhat < soThuBa &&
    soThuHai > soThuBa
  ) {
    sapXep = `${soThuNhat}, ${soThuBa}, ${soThuHai}`;
  } else if (
    soThuNhat < soThuHai &&
    soThuNhat < soThuBa &&
    soThuHai < soThuBa
  ) {
    sapXep = `${soThuNhat}, ${soThuHai}, ${soThuBa} `;
  } else {
    sapXep = `${soThuHai}, ${soThuNhat}, ${soThuBa}`;
  }
  document.getElementById("ketQua").innerHTML = `${sapXep}`;
};
